# Petit exemple de programme en go avec des tests

## Pour lancer les tests unitaires:

go test -v

## Pour lancer les tests de performance:

go test -bench="."

## Pour connaitre la couverture de test

go test -cover

